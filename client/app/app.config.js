function AppConfig($logProvider, $stateProvider, $locationProvider, 
                    $urlRouterProvider, $mdThemingProvider) {
  'ngInject';

  // themes for AngularJS material
  $mdThemingProvider.theme('default')
    .primaryPalette('blue-grey')
    .accentPalette('green',{
      'default':'400'
    })
    .warnPalette('red')

   $locationProvider.html5Mode(true);


  $stateProvider
    .state('app', {
      component: 'app'
    });

  $urlRouterProvider.otherwise('/');
}

export default AppConfig;