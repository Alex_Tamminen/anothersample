import angular from 'angular';
import Navbar from './navbar/navbar';
import Header from './header/header'
import commonServicesModule from './services/service';

// module which contains all components, services that are common for all components
let commonModule = angular.module('app.common', [
  Header, Navbar, commonServicesModule, 
])
.name;

export default commonModule;
