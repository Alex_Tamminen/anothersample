
import './navbar.scss';

class NavbarController {
  constructor(ProductService, $state) {
    'ngInject';
    this.shoppingcart = []; // local variable for shopping cart content
    this.categories = []; // the different categories for products, fetched from service
    this.totalSum = 0; // total sum of shopping cart, fetched from service
    this._ProductService = ProductService; // all actions regarding products and shopping cart handled in service
    // this.searchTerm = ""; 
    this._$state = $state;   // to be able to navigate
  }

  $onInit() { // fired upon navigation to component
    this.shoppingcart = this._ProductService.getShoppingcart(); // fetch current shopping cart content
    this.totalSum = this._ProductService.getTotalSum(); // fetch current sum of shopping cart content
    this.categories = this._ProductService.getCategories() // fetch categories for products to show in navbar, where user can choose between product categories
  }

  getTotalSum() { // fetches total sum of shopping cart content
    return this._ProductService.getTotalSum()
  }

  getProductAmount() { // fetches total amount of products in shoppin cart
    return this._ProductService.getProductAmount();
  }
}

let navbarComponent = {
  bindings: {},
  template: `

  <md-content>
    <md-toolbar style="height:20px" class="md-hue-1" ng-mouseleave="$mdMenu.close()">

        <!-- Hide dropdown menu on mouseleave. KNOWN BUG: doesn't hide when user moves directly upwards -->
        <div ng-mouseleave="$mdMenu.close()" >

          <!-- 
            Each menu is one item in navbar. Only 'products' is a drop down menu, other ones navigate
            to different pages when clicked.         
           -->
          <md-menu-bar >
            <md-menu > 
              <button class="whiteButton" ng-click="$ctrl._$state.go('home')">
                Home
              </button>

              <!-- Need to use dummy boolean here, since otherwise it complains for not having md-menu-content -->
              <md-menu-content ng-hide="!something"></md-menu-content>
            </md-menu>

            <md-menu > 
              <button class="whiteButton" ng-click="$mdMenu.open()">
                Products
              </button>
              <md-menu-content ng-mouseleave="$mdMenu.close()">

                <!-- iterate product categories and move to specific category or show all -->
                <md-menu-item ng-repeat="category in $ctrl.categories">
                  <md-button ui-sref="products({category:category})">
                    {{category}}
                  </md-button>
                </md-menu-item>
              </md-menu-content>
            </md-menu>

            <md-menu > 
              <button class="whiteButton" ng-click="$ctrl._$state.go('about')">
                About Us
              </button>
              <!-- Need to use dummy here, since otherwise it complains for not having md-menu-content -->
              <md-menu-content ng-hide="!something"></md-menu-content>
            </md-menu>

            <md-menu > 
              <button class="whiteButton" ng-click="$ctrl._$state.go('shoppingcart')">
                Checkout
              </button>
              <!-- Need to use dummy here, since otherwise it complains for not having md-menu-content -->
              <md-menu-content ng-hide="!something"></md-menu-content>
            </md-menu>
          </md-menu-bar>
        </div>
    </md-toolbar>
    </md-content>

  `,
  controller: NavbarController
};

export default navbarComponent;
