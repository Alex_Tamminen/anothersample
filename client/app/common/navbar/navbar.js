import angular from 'angular';
import uiRouter from 'angular-ui-router';
import navbarComponent from './navbar.component';

// separate module and declared in module 'common' (index.js)
let navbarModule = angular.module('navbar', [
  uiRouter
])

.component('navbar', navbarComponent)
.name;

export default navbarModule;


