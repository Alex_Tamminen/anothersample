
import './header.scss';

class HeaderController {
  constructor(ProductService, $state) {
    'ngInject';
    this._$state = $state; // to be able to navigate
    this.shoppingcart = []; // local variable for items in shopping cart
    this.totalSum = 0; // total sum of items in shopping cart
    this._ProductService = ProductService; // handle all action of shopping cart content in service
    // this.searchTerm = "";   
  }

  $onInit() { // fired upon navigating to component
    this.shoppingcart = this._ProductService.getShoppingcart(); // retrieve current shoppingcart content
    this.totalSum = this._ProductService.getTotalSum() // retrieve current total sum of shopping cart
  }

  getTotalSum() { // fetches the total sum
    return this._ProductService.getTotalSum()
  }

  getProductAmount() { // fetches amount of items in shoppingcart
    return this._ProductService.getProductAmount();
  }

  goToShoppingcart() { // navigates to shopping cart page
    this._$state.go('shoppingcart');
  }
}

let headerComponent = {
  bindings: {},
  template: `

  <md-toolbar class="md-tall">
    <div class="md-toolbar-tools">

      <!-- page header -->
      <h1>Felines unite!</h1>

        <!-- Makes space between title and shopping cart -->
        <span flex></span>

        <!-- 
          Shows the content and total sum in shopping cart. 
          PLEASE NOTICE! Here function called in template, IRL this could not be done, 
          because it would be called on each change detection 
          and cause a never ending looping!
        -->
        <fieldset ng-click="$ctrl.goToShoppingcart()">
          <span class="md-headline">{{$ctrl.getTotalSum()}} €</span><br>
          <span class="md-subhead">{{$ctrl.getProductAmount()}} Products</span>
        </fieldset>
    </div>
  </md-toolbar>
  `,
  controller: HeaderController
};

export default headerComponent;
