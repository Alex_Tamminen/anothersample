

// product, all include unique id, as well as name,description,PPU,quantity,total price based on quantity and PPU and finally a product category
const products = [
    {"id":1,"name":"Door Stopper","description":"Humans tend to close doors. This is not acceptable and our patented door stopper is the solution for you! 100% human-proof!","price":15,"quantity":0,"total":0,"category":"Home Improvement"},    
    {"id":2,"name":"Leash and Collar","description":"A comfortable collar with a fancy but sturdy leash for taking your human for a walk","price":40,"quantity":0,"total":0,"category":"Activity"},
    {"id":3,"name":"Clicker","description":"Easily train your Human with this clicker. You also get a bag of treats to reward your human for good behavior","price":13,"quantity":0,"total":0,"category":"Activity"},
    {"id":4,"name":"Exercise Wheel","description":"Human-sized exercise wheel for the days you feel too lazy to walk your Human.","price":169,"quantity":0,"total":0,"category":"Activity"},
    {"id":5,"name":"Body Blanket","description":"As you have noticed, Humans are sadly hairless. Buy this fancy body blanket to keep your human warm!","price":20,"quantity":0,"total":0,"category":"Body Coverage"},
    {"id":6,"name":"Attachable Thumb","description":"Humans have thumbs, they can do amazing things with thumbs! With this product, you can do the same!","price":60,"quantity":0,"total":0,"category":"Other"},
    {"id":7,"name":"Bag of treats","description":"Most humans consider weird sweet things as treats, buy a bag of Human treats for your Human to reward for good behavior!","price":5,"quantity":0,"total":0,"category":"Other"},
    {"id":8,"name":"Ringing Bell","description":"Train your Human to respond to this ringing bell whenever you want something. The clicker is useful together with this to train your Human.","price":5,"quantity":0,"total":0,"category":"Other"},
    {"id":9,"name":"Item Card","description":"Unfortunately humans are not smart enough to understand our language. With this pointing card with pictures you can show your Human what it is that you want","price":4,"quantity":0,"total":0,"category":"Other"},
    {"id":10,"name":"Cat Ladder","description":"Humans tend to try and hide things where we can't access, even though we are good jumpers. This foldable ladder together with the attachable thumb will help you reach those places!","price":30,"quantity":0,"total":0,"category":"Home Improvement"}

]

class ProductService {

    products = [] // holds all products from DB (here static constant)
    shoppingcart = []; // content of shopping cart shared among all components!
    totalSum = 0; // total price of shopping cart shared among all components!
    productCategories = ["All","Activity","Home Improvement","Body Coverage", "Other"] // all product categories

    constructor() {
        this.products = products // assign all products from constant above
     }

    // returns all product categories or based on what user has clicked in drop down (in navbar)
    getProducts(category) {
       if(category === "All") {
            return this.products;
       } else {
            // filter products based on category user has chosen
            var filteredProducts = [];
            filteredProducts = this.products.filter(product => product.category === category)
            return filteredProducts;
       }
    }


    // parameter product is the chosen product, which we want to add or remove quantity
    countProduct(product, addOrRemove) {
        // add quantity and count total
        if(addOrRemove === '+') {
            product.quantity++; // increment product quantity
            // counts total price sum of specific product
            this.countTotal(product);
            // where we add item to cart or if exists update quantity
            this.addToCart(product);
          // subtract quantity  
        } else if(addOrRemove === '-') {
            // check if user has clicked minus when product quantity is 0, if so, we don't show quantity as -1
            if(product.quantity === 0 || product.quantity === -1 ) {       
                product.quantity = 0;
                product.total = 0;
                return;
            // above condition is falsy, so means that there is as quantity was before at least 1, so it can be subtracted
            } else {
                product.quantity--
                this.countTotal(product);
                // where we delete item to cart or if exists update quantity
                this.deleteFromCart(product);
            }
        }
    }

    // counts total price of product
    countTotal(product) {
        product.total = product.price * product.quantity;
    }

    // push product to cart if not exists, or update chosen product quantity
    addToCart(product) {
        let index = this.shoppingcart.indexOf(product);
        if(index === -1) {
            this.shoppingcart.push(product);
        } else {
            this.shoppingcart[index] = product;
        }
    }

    // used in components to get the current state of shopping cart
    getShoppingcart() {
       return this.shoppingcart;
    }

    // delete item from cart if exists, or update chosen product quantity
    deleteFromCart(product) {    
        let index = this.shoppingcart.indexOf(product);    
        if(product.quantity === 0) {
            this.shoppingcart.splice(index, 1);
        } else {
            this.shoppingcart[index] = product;
        }
    }

    // IRL we have a list of categories in DB, which we would fetch
    getCategories() {
        return this.productCategories;
    }

    // used in components to get the current total sum of items in shopping cart
    getTotalSum() {    
        var totalSum = 0;

        // iterate shopping cart to count total sum of current state of shopping cart
        for(var i=0; i<this.shoppingcart.length; i++) {
            totalSum += this.shoppingcart[i].total;
        }
        return totalSum;
    }

    // used in component to get the current count of items in shopping cart
    getProductAmount() {
        var productAmount = 0;

        // iterate shopping cart to get amount of items in array and count the total amount of items
        for(var i=0; i<this.shoppingcart.length; i++) {
            productAmount += this.shoppingcart[i].quantity;
        }
        return productAmount;        
    }

    // used in shopping cart component when user has clicked to delete all items of chosen product
    removeFromCart(product) {
        let index = this.shoppingcart.indexOf(product);
        let index1 = this.products.indexOf(product);
        // set the product quantity of chosen product to zero, so that all components have the correct quantity
        this.products[index1].quantity = 0; 
        this.shoppingcart.splice(index, 1);    
    }

}

export default ProductService;
