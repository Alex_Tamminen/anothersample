import angular from 'angular';
import ProductService from './product.service';

// separate module and declared in module 'common' (index.js)
let commonServicesModule = angular.module('services', [])
  .service('ProductService', ProductService)
  .name;

export default commonServicesModule;