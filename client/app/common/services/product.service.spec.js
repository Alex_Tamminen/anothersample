import commonServicesModule from './service'
import ProductService from './product.service'
import 'angular-mocks';
import 'angular';


describe('ProductService', () => {

  let service;

  beforeEach(() => {
    angular.mock.module(commonServicesModule);
    angular.mock.inject(($injector) => {
      service = $injector.get('ProductService');
    })
    
  });

  it('should be defined', () => { 
    expect(service).to.be.defined;
  }); 

  describe('Service variables', () => {

    it('shoppingcart: array should initially be empty', () => { 
      assert.equal(service.shoppingcart.length, 0)
    }); 

    it('products: array should hold 10 products', () => { 
      assert.equal(service.products.length, 10)
    }); 

    it('productCategories: should contain 5 product categories (string)', () => { 
      assert.equal(service.productCategories.length, 5)
    }); 

    it('totalSum: should initially be 0', () => { 
      assert.equal(service.totalSum, 0)
    }); 
  })

  describe('getProducts()', () => {
    it('should return all product categories when "All" category is chosen', () => { 
      let emptyArr;
      emptyArr = service.getProducts('All')
      assert.equal(emptyArr.length, 10)
    }); 
    it('should return filtered product categories based on user choice', () => { 
      let emptyArr;
      emptyArr = service.getProducts('Activity')
      assert.equal(emptyArr.length, 3)
    }); 
  })

  describe('countProduct()', () => {
    it('should increment quantity of product if user clicked plus button', () => { 
      let product = {"id":3,"name":"Clicker","description":"","price":1,"quantity":2,"total":0,"category":"Activity"};
      service.countProduct(product, '+')
      assert.equal(product.quantity, 3)
      // below not working!!
      // expect(service.countTotal(product)).toHaveBeenCalled();
      // should also include check that addToCart should have been called

    }); 
    it('shoulduld subtract quantity of product if user clicked minus button', () => { 
      let product = {"id":3,"name":"Clicker","description":"","price":1,"quantity":2,"total":0,"category":"Activity"};
      service.countProduct(product, '-')
      assert.equal(product.quantity, 1)
      // below not working!!
      // expect(service.countTotal()).toHaveBeenCalled();
      // should also incluce check that removeFromCart has been called
    }); 

  })  

  describe('countTotal()', () => {
    it(' should count total sum of of a product based on quantity * PPU', () => { 
      let product = {"id":3,"name":"Clicker","description":"","price":1,"quantity":2,"total":0,"category":"Activity"};
      assert.equal(product.total, 0)
      service.countTotal(product)
      assert.equal(product.total, 2)
    }); 
  })
 
  describe('addToCart()', () => {
    it('should push new product to cart if not already in cart', () => { 
      let product = {"id":3,"name":"Clicker","description":"","price":1,"quantity":2,"total":0,"category":"Activity"};
      assert.equal(service.shoppingcart.length, 0)      
      service.addToCart(product)
      assert.equal(service.shoppingcart.length, 1)
    }); 

    it('should update existing product (quantity) in cart', () => {
      assert.equal(service.shoppingcart.length, 0)
      let product = {"id":3,"name":"Clicker","description":"","price":1,"quantity":2,"total":0,"category":"Activity"};
      service.addToCart(product);
      assert.equal(service.shoppingcart.length, 1)

      product.quantity++
      service.addToCart(product)
      assert.equal(service.shoppingcart[0].quantity, 3)
    });

  })

  describe('getShoppingcart()', () => {
    it('should return current shoppingcart array', () => { 
      let testArr;
      testArr = service.getShoppingcart()
      assert.equal(testArr.length, 0)
    }); 
  })

  describe('deleteFromCart()', () => {
    it('should remove product from cart if chosen product quantity is 0', () => { 
      let product = {"id":3,"name":"Clicker","description":"","price":1,"quantity":0,"total":0,"category":"Activity"};
      service.addToCart(product)
      assert.equal(service.shoppingcart.length, 1)
      service.deleteFromCart(product)
      service.shoppingcart.indexOf(product).should.equal(-1);
    }); 

    it('should update product in cart if quantity of product is other than 0', () => { 
      let product = {"id":3,"name":"Clicker","description":"","price":1,"quantity":1,"total":0,"category":"Activity"};
      service.addToCart(product)
      assert.equal(service.shoppingcart.length, 1)
      service.deleteFromCart(product)
      service.shoppingcart.indexOf(product).should.equal(0);
    }); 
  })

  describe('getCategories()', () => {
    it('should return array with 5 product categories', () => { 
      let testArr;
      testArr = service.getCategories()
      assert.equal(testArr.length, 5)
    }); 
  })

  describe('getTotalSum()', () => {
    it('should return total sum (price) of items in shoppingcart', () => { 
      let product = {"id":3,"name":"Clicker","description":"","price":1,"quantity":2,"total":2,"category":"Activity"};
      service.addToCart(product)
      let total = 0;
      total = service.getTotalSum()
      assert.equal(total, 2)
    }); 
  })

  describe('getProductAmount()', () => {
    it('should return number of products in shoppingcart', () => { 
      let product = {"id":3,"name":"Clicker","description":"","price":1,"quantity":2,"total":0,"category":"Activity"};
      service.addToCart(product)
      let testArr;
      testArr = service.getProductAmount()
      assert.equal(testArr, 2)
    }); 
  })

  // not working!
/*
  describe('removeFromCart()', () => {
    it('should completely remove item from shoppingcart', () => { 
      let product = {"id":3,"name":"Clicker","description":"Easily train your Human with this clicker.","price":13,"quantity":0,"total":0,"category":"Activity"}
      service.addToCart(product)
      assert.equal(service.shoppingcart.length, 1)
      // assert.equal(service.products.length, 10)

      service.removeFromCart(product)
      assert.equal(service.shoppingcart.length, 0)
      // assert.equal(service.products[2].quantity, 0)
    }); 
*/
  })



