import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Common from './common/index';
import Components from './components/index';
import AppComponent from './app.component';
import AppConfig from './app.config'
import ngMaterial from 'angular-material';
import 'angular-material/angular-material.css';
// import 'bootstrap/dist/css/bootstrap.css';
import 'angular-material/angular-material.css';


angular.module('app', [
    uiRouter,
    Common,
    Components,
    ngMaterial
  ])
  .config(AppConfig) 

  .component('app', AppComponent);
