import angular from 'angular';
import Products from './products/products';
import About from './about/about';
import Shoppingcart from './shoppingcart/shoppingcart';
import Home from './home/home'

let componentModule = angular.module('app.components', [
  Home,
  Products,
  About,
  Shoppingcart
])

.name;

export default componentModule;
