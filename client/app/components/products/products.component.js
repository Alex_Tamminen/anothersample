
import './products.scss';

class ProductsController {
  constructor(ProductService, $stateParams) {
    'ngInject';
    this.products = []; // local variable for all products
    this._ProductService = ProductService // handles all actions on for shopping cart and fetches products
    this.shoppingcart = []; // local variable for shopping cart content
    this.category = $stateParams.category // route param. It is the name of the category chosen
  }

  $onInit() {
    // get products based on the category chosen, which comes as a route param
    this.products = this._ProductService.getProducts(this.category);
    // this.shoppingcart = this._ProductService.getShoppingcart()
  }

  // user has chosen to increment or subtract product quantity, sent to service to handle adding or removing
  countProduct(product, quantity) {
    this._ProductService.countProduct(product, quantity)
  }

}

const productsComponent = {
  controller: ProductsController,
  template: `

  
  <div class="paddings">
  <!-- Show the chosen category as title -->
  <h1>{{$ctrl.category.toUpperCase()}}: </h1>
  <div class="gridListdemoBasicUsage">
    <div  layout="row" layout-wrap>

      <!-- iterate all products and show as cards -->
      <md-card ng-repeat="product in $ctrl.products" style="width:350px">
        <md-card-title>
          <md-card-title-text>
            <span class="md-headline" style="color:#193366"><b>{{product.name}}</b></span><br><br>
            <span class="md-subhead">{{product.description}}</span>
          </md-card-title-text>
          <md-card-title-media>         
            <div class="md-media-sm card-media">
                <h3 style="color:#193366"><b>PPU: <br>{{product.price}} €</b></h3>
            </div> 
          </md-card-title-media>
        </md-card-title>

        <!-- Action buttons for deleting or adding quantity and showing the quantity in the center -->
        <md-card-title-text layout-align="center center" layout="row" layout-align="center">
          <md-button  class="md-fab" style="background-color: #ff3333" ng-click="$ctrl.countProduct(product, '-')"><b>-</b></md-button>
          <h2 class="spanPadding">{{product.quantity}}</h2>
          <md-button class="md-fab" style="background-color: #009933" ng-click="$ctrl.countProduct(product, '+')"><b>+</b></md-button>
        </md-card-title-text>
      </md-card>
    </div>
  </div>
  </div>
  `
}

export default productsComponent;
