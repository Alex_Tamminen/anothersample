import angular from 'angular';
import uiRouter from 'angular-ui-router';
import productsComponent from './products.component';

let productsModule = angular.module('products', [
  uiRouter
])

.config(($stateProvider, $urlRouterProvider) => {
  "ngInject";
  $stateProvider // setting route
    .state('products', {
      url: '/products/:category',
      component: 'products'
    });
})

.component('products', productsComponent)
.name;

export default productsModule;
