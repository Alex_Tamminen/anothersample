import angular from 'angular';
import uiRouter from 'angular-ui-router';
import shoppingcartComponent from './shoppingcart.component';

let shoppingcartModule = angular.module('shoppingcart', [
  uiRouter
])

.config(($stateProvider) => { // define route
  "ngInject";
  $stateProvider
    .state('shoppingcart', {
      url: '/shoppingcart',
      component: 'shoppingcart'
    });
})

.component('shoppingcart', shoppingcartComponent)
  
.name;

export default shoppingcartModule;
