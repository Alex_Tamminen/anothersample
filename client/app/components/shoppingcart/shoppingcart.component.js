
import './shoppingcart.scss';

class ShoppingcartController {
  constructor(ProductService) {
      'ngInject';
       this._ProductService = ProductService; // handles all action to shopping cart
       this.shoppingcart; // local variable for shopping cart content
  }

  $onInit() {
    // get all items in shopping cart
    this.shoppingcart = this._ProductService.getShoppingcart();
  }

  // user can increment or subtract items in cart one by one
  countProduct(product, action) {
    this._ProductService.countProduct(product, action)
  }

  // complete removs the item from cart
  removeFromCart(product) {
    this._ProductService.removeFromCart(product)
  }

}

let shoppingcartComponent = {
  bindings: {},
  template: `

  <div class="paddings">

  <md-toolbar class="smaller">
    <h2>Shopping Cart</h2>
  </md-toolbar>

  <h2 ng-if="$ctrl.shoppingcart.length == 0">OH NO! Your shopping cart is empty!</h2>

  <md-content>
    <md-list>
      <md-list-item class="md-3-line" ng-repeat="product in $ctrl.shoppingcart">
        <img class="md-avatar">
        <div class="md-list-item-text">
          <h3>{{product.name}}</h3>
          <h4>{{product.total}} €</h4>
          <p>{{product.price}} € / unit</p>

          <!-- Buttons for incrementing, subtracting or deleting all units of a product -->
          <md-button style="background-color: #ff3333" class="md-fab md-warn; md-secondary" ng-click="$ctrl.countProduct(product, '-')">-</md-button>
          <md-button class="md-secondary"><h2>{{product.quantity}}</h2></md-button>
          <md-button style="background-color: #009933" class="md-fab md-warn; md-secondary" ng-click="$ctrl.countProduct(product, '+')">+</md-button>
          <md-button class="md-secondary" ng-click="$ctrl.removeFromCart(product)" ><h4>DELETE</h4></md-button>
        </div>
        <md-divider md-inset ng-if="!$last"></md-divider>
      </md-list-item>
    </md-list>
  </md-content>

  </div>
  `,
  controller: ShoppingcartController
};

export default shoppingcartComponent;
