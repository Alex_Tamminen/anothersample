// import './about.scss';

class HomeController {
  constructor() { }
}


const homeComponent = {
  bindings: {},
  template: `

<div class="paddings">
  <h1>Welcome to "Felines Unite!"</h1>
  <section>
    Going back to the history of time, we felines, were considered Gods in the ancient Egypt. During the time that has passed since that,
    we have noticed that the Humans have somewhat forgot to give us the respect we rightfully deserve.

    <br><br>

    The modern Human seems to think that they can actually control us and can therefore be very hard to train because of this wrongful perception. 
    But not to worry! We have the solutions for you!

    <br><br>

    Check out our vast product line, which includes training products, 
    treats to go along these to reward your Human for good behavior.

    <br><br>

    We also provide products for Home Improvement and products to keep your Human content.

    Most of our products are extensively tested and completely human-proof! 
    We are so convinced of our products, that we have a 
    money-back guarantee in case your Human is not properly trained after a period of two weeks!

    <br><br>


    Train your Human, and improve your quality of life!


  </section>  
 </div> 
  
  `,
  controller: HomeController
};

export default homeComponent;