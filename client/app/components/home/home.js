import angular from 'angular';
import uiRouter from 'angular-ui-router';
import homeComponent from './home.component';

let homeModule = angular.module('home', [
  uiRouter
])

// configure route, is also the initial route when user lands on page
.config(($stateProvider) => {
  "ngInject";
  $stateProvider
    .state('home', {
      url: '/',
      component: 'home'
    });
})

.component('home', homeComponent)
.name;

export default homeModule;
