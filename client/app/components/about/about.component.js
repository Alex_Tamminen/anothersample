import './about.scss';

class AboutController {
  constructor() {

  }
}


const aboutComponent = {
  bindings: {},
  template: `


<div class="paddings">
  <h1>About Us</h1>

  <section>

  We started with ideas out of personal needs when we realized our Human
  was actually try to make decisions that directly affected our life in some way.

  <br><br>

  The <b>negative</b> things we are talking about is for example deciding what time we are given food and that 
  the Human can leave the home whenever THEY decide! We needed to put an end to it, 
  and that motivated us to come up with products that would help us. And so we did.
  
  <br><br>

  After making our products, we noticed some big improvements in the Human's behavior and it occured to us
  that why keep this to ourselves, why not share it with our feline friends all over the world??

  <br><br>

  So now we are here, our mission is to improve your quality of life with our patented products.
  <br><br>
  Sincerely yours,
  <br><br>

  Lissie and Lucky.

  <br><br>

  <div layout-align="center center" layout="row" layout-align="center" >
    <img class="resize" src="app/public/LissieLucky.png" />
  </div>
    
  </section>  

  
 </div> 
  
  
  `,
  controller: AboutController
};

export default aboutComponent;
